# Express Exam

## Requirements

* [nodejs v10.16.3](http://nodejs.org)
* [npm v6.14.4](https://www.npmjs.com/get-npm)

## Objetives

You must build a simple [REST](https://en.wikipedia.org/wiki/Representational_state_transfer) 
API backend using [Express js](https://expressjs.com/es/), [Connect Js](https://github.com/senchalabs/connect#readme) 
or any other web framework for node.

this repo has already installed __Express Js__ and __axios__ 

## Quetions

### 1. GET Average

You must calculate the average from a list of integers and return its average

the path can be _/average_

You may use any verb and the answer must be like

```JSON
{
  "average": 1234
}
```

Where, __1234__ is the calculated average

### 2. GET Wheather

You must to get the wheather from a city from another api.

the path can be _/weather/{cuntry}/{city}_ as a GET verb

The Api to be is defined by

```javascript
const axios = require("axios");

//
// Weather by ZIP Code. (zipcode, country)
//
axios({
  "method": "GET",
  "url": "https://api.openweathermap.org/data/2.5/weather",
  "params": {
    "appid": "b3089a1608bf55629de9e36f6403da53",
    "zip": "28231,ES"
  }
})
  .then((response) => {
    console.log(response)
  })
  .catch((error) => {
    console.log(error)
  })

//
//  Wheather by 
//
axios({
  "method": "GET",
  "url": "https://api.openweathermap.org/data/2.5/weather",
  "params": {
    "q": "Las Rozas,ES",
    "appid": "b3089a1608bf55629de9e36f6403da53"
  }
})
  .then((response) => {
    console.log(response.data)
  })
  .catch((error) => {
    console.log(error)
  })
```

__notice__ that all the temps are returning in _kelvin_ so you must to transform it to _celsius

```javascript
const temperatureC = (temperatureK) => temperatureK - 273.15;
```

### 3. Post data

the path can be _/login_

you must post data for a login form. the values required are

Field       | Type      | Required
----------- | --------- | ------------
`email`     | email     | `true`
`password`  | password  | `true`
`2fa`       | number    | `false`

if is possible type and required can be validated using a __middleware__
