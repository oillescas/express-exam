const express = require('express');
const app = express();
const bodyParser = require('body-parser')

const weather = require('./weader.js');
const temperatureC = (temperatureK) => temperatureK - 273.15;

app.use(bodyParser.json())

app.post('/average', function (req, res) {
  const values = req.body;
  const suma = values.reduce((init, item)=>{
    return init+item;
  }, 0);
  res.send({
    "average": suma/(values.length)
  });
});

app.get('/weather/:country/:city', async function(req, res){
  const country = req.params.country;
  const city = req.params.city;
  let wResponse = {};
  try {
    wResponse = await weather.byLocaleName('Madrid', 'ES');
    const temp_max = temperatureC(wResponse.data.main.temp_max);
    const temp_min = temperatureC(wResponse.data.main.temp_min);
    const temp = temperatureC(wResponse.data.main.temp);

    res.send({
      temp_max, 
      temp_min,
      temp
    });
  } catch (error) {
    res.send(error);
  }
  
});

app.listen(3000, function () {
  console.log('Example app listening on port 3000!');
});