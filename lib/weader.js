const axios = require("axios");


function byZipCode(zipCode, country) {
  //
  // Weather by ZIP Code. (zipcode, country)
  //
  return axios({
    "method": "GET",
    "url": "https://api.openweathermap.org/data/2.5/weather",
    "params": {
      "appid": "b3089a1608bf55629de9e36f6403da53",
      "zip": `${zipCode},${country}`
    }
  })

};

function byLocaleName(name, country) {
  //
  //  Wheather by 
  //
  return axios({
    "method": "GET",
    "url": "https://api.openweathermap.org/data/2.5/weather",
    "params": {
      "q": `${name},${country}`,
      "appid": "b3089a1608bf55629de9e36f6403da53"
    }
  })
}

module.exports = {
  byZipCode,
  byLocaleName
};